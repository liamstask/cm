
# cm

emulator for cortex-m series devices

[![Build Status](https://drone.io/bitbucket.org/liamstask/cm/status.png)](https://drone.io/bitbucket.org/liamstask/cm/latest)

this is just for fun at the moment, and is not close to being complete enough to be useful, but the goal is to provide reasonably faithful emulation for popular hardware (STM32, mbed, etc) including peripherals, making it easier to prototype, test, and develop applications for these devices.

one day, libcm may be able to be integrated into your application to embed an emulator.

## build

cm is built with [tup](http://gittup.org/tup/)

from the root directory:

    $ tup
    $ ... tup inits and builds ...
    $ ./cmd/cm -v

otherwise, no external dependencies (yet).
