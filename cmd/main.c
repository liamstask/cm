
#include "cm.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sysexits.h>

static void threadFunc(struct Stm32F0 *stm32);

static void usage()
{
    fprintf(stderr, "usage: cm <bin | elf>\n");
}

static void version()
{
    printf("cm v0.1\n");
}

int main(int argc, char **argv)
{
    const char *firmwarePath = NULL;

    int i;
    for (i = 1; i < argc; i++) {
        if (!strncmp(argv[i], "-v", strlen("-v"))) {
            version();
            exit(0);
        }

        // default
        if (!firmwarePath) {
            firmwarePath = argv[i];
        }
    }

    if (!firmwarePath) {
        fprintf(stderr, "err - must specify binary to execute\n");
        usage();
        exit(EX_USAGE);
    }

    // lib initialization
    uint32_t clock = 16000000;
    unsigned timestep = 10;
    cmInit(clock, timestep);

    // target initialization
    struct Stm32F0 stm32;
    if (!stm32F0Init(&stm32, firmwarePath)) {
        exit(EX_NOINPUT);
    }

    threadFunc(&stm32);

    cmMemDeinit(&stm32.flash);

    return 0;
}

void threadFunc(struct Stm32F0 *stm32)
{
    /*
     * Main loop - if needed, this can be run in a separate thread.
     */

    VirtualTime *vt = cmSystemTime();

    TimeGovernor gov;
    timeGovStart(&gov, vt);

    bool threadRunning = true;  // XXX: provide a real way to stop thread
    while (threadRunning) {

        unsigned batch = vTimeTimestepTicks(vt);
        while (batch--) {
            vTimeTick(vt, 1);
            stm32F0Tick(stm32);
        }

        timeGovStep(&gov);
    }
}
