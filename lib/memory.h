#ifndef MEMORY_H
#define MEMORY_H

#include <stdint.h>
#include <stdbool.h>

struct Memory {
    uint8_t *bytes;
    unsigned capacity;  // size of memory pointed to by bytes
    unsigned base;      //
    unsigned pageSize;
    int fileHandle;
    uint32_t flags;
};

bool cmMemInit(struct Memory *m, const char *filepath);
void cmMemDeinit(struct Memory *m);

#endif // MEMORY_H
