
#include "armv6m.h"
#include "mem.h"
#include "armv6m-mem.h"
#include "thumb2-emu.h"
#include "thumb-encoding.h"
#include "memory.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

static void armv6mExecute16(CortexMCore *cpu, uint16_t instr);
static void armv6mExecute32(CortexMCore *cpu, uint32_t instr);

static const struct CortexMVTable ARMv6M_vtable = {
    armv6mRead8,
    armv6mRead16,
    armv6mRead32,
    armv6mWrite8,
    armv6mWrite16,
    armv6mWrite32,
    armv6mExecute16,
    armv6mExecute32
};

void cmInitARMv6M(CortexMCore *cpu)
{
    cpu->vtable = &ARMv6M_vtable;
    cpu->name = "armv6-m";
    cpu->coreType = CoreARMv6M;
}

void armv6mExecute16(CortexMCore *cpu, uint16_t instr)
{
    if ((instr & AluMask) == AluTest) {
        // lsl, lsr, asr, add, sub, mov, cmp
        // take bits [13:11] to group these
        uint8_t prefix = (instr >> 11) & 0x7;
        switch (prefix) {
        case 0: emulateLSLImm(cpu, instr); return;
        case 1: emulateLSRImm(cpu, instr); return;
        case 2: emulateASRImm(cpu, instr); return;
        case 3: {
            uint8_t subop = (instr >> 9) & 0x3;
            switch (subop) {
            case 0: emulateADDReg(cpu, instr);  return;
            case 1: emulateSUBReg(cpu, instr);  return;
            case 2: emulateADD3Imm(cpu, instr); return;
            case 3: emulateSUB3Imm(cpu, instr); return;
            }
        }
        case 4: emulateMovImm(cpu, instr);  return;
        case 5: emulateCmpImm(cpu, instr);  return;
        case 6: emulateADD8Imm(cpu, instr); return;
        case 7: emulateSUB8Imm(cpu, instr); return;
        }
        assert(0 && "unhandled ALU instruction!");
    }

    if ((instr & DataProcMask) == DataProcTest) {
        uint8_t opcode = (instr >> 6) & 0xf;
        switch (opcode) {
        case 0:  emulateANDReg(cpu, instr); return;
        case 1:  emulateEORReg(cpu, instr); return;
        case 2:  emulateLSLReg(cpu, instr); return;
        case 3:  emulateLSRReg(cpu, instr); return;
        case 4:  emulateASRReg(cpu, instr); return;
        case 5:  emulateADCReg(cpu, instr); return;
        case 6:  emulateSBCReg(cpu, instr); return;
        case 7:  emulateRORReg(cpu, instr); return;
        case 8:  emulateTSTReg(cpu, instr); return;
        case 9:  emulateRSBImm(cpu, instr); return;
        case 10: emulateCMPReg(cpu, instr); return;
        case 11: emulateCMNReg(cpu, instr); return;
        case 12: emulateORRReg(cpu, instr); return;
        case 13: emulateMUL(cpu, instr);    return;
        case 14: emulateBICReg(cpu, instr); return;
        case 15: emulateMVNReg(cpu, instr); return;
        }
    }

    if ((instr & SpecialDataMask) == SpecialDataTest) {
        uint8_t opcode = (instr >> 6) & 0xf;
        uint8_t subop = (opcode >> 2) & 0x3;

        switch (subop) {
        case 0: emulateADDReg(cpu, instr);  return;
        case 1:
            if ((opcode & 0x3) == 0) {
                assert(0 && "UNPREDICTABLE");
                // emulateFault();
            }
            emulateCMPReg(cpu, instr); return;
        case 2: emulateMOV(cpu, instr); return;
        case 3:
            if (opcode & (1 << 1)) {
                emulateBX(cpu, instr); return;
            } else {
                emulateBLX(cpu, instr); return;
            }
        }
    }

    if ((instr & LoadStoreMask) == LoadStoreTest) {
        uint8_t opcode = (instr >> 9) & 0x7;
        switch (opcode) {
        case 0: emulateSTRReg(cpu, instr);   return;
        case 1: emulateSTRHReg(cpu, instr);  return;
        case 2: emulateSTRBReg(cpu, instr);  return;
        case 3: emulateLDRSBReg(cpu, instr); return;
        case 4: emulateLDRReg(cpu, instr);   return;
        case 5: emulateLDRHReg(cpu, instr);  return;
        case 6: emulateLDRBReg(cpu, instr);  return;
        case 7: emulateLDRSHReg(cpu, instr); return;
        }
    }

    if ((instr & LoadStore2Mask) == LoadStore2Test) {
        uint8_t opcode = (instr >> 9) & 0x7;
        switch (opcode) {

        }
    }

    if ((instr & LoadStore3Mask) == LoadStore3Test) {
        uint8_t opcode = (instr >> 9) & 0x7;
        switch (opcode) {

        }
    }

    fprintf(stderr, "CM: unhandled 16bit instruction: 0x%x\n", instr);
    assert(false);
    // emulateFault();
}

void armv6mExecute32(CortexMCore *cpu, uint32_t instr)
{
    // XXX: implement

    fprintf(stderr, "CM: unhandled 32bit instruction: 0x%x\n", instr);
    assert(false);
    // emulateFault();
}


