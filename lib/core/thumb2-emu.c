#include "thumb2-emu.h"
#include "mem.h"
#include "thumb-encoding.h"

#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

/*
 * Thumb-2 Instruction Emulation
 */


/////////////////////////////////////////////////
//          Flags and Status Register
/////////////////////////////////////////////////


static inline bool getNeg(reg_t cpsr) {
    return (cpsr >> 31) & 1;
}
static inline void setNeg(CortexMCore *cpu, bool f) {
    if (f)
        cpu->cpsr |= (1 << 31);
    else
        cpu->cpsr &= ~(1 << 31);
}

static inline bool getZero(reg_t cpsr) {
    return (cpsr >> 30) & 1;
}
static inline void setZero(CortexMCore *cpu, bool f) {
    if (f)
        cpu->cpsr |= (1 << 30);
    else
        cpu->cpsr &= ~(1 << 30);
}

static inline bool getCarry(reg_t cpsr) {
    return (cpsr >> 29) & 1;
}
static inline void setCarry(CortexMCore *cpu, bool f) {
    if (f)
        cpu->cpsr |= (1 << 29);
    else
        cpu->cpsr &= ~(1 << 29);
}

static inline int getOverflow(reg_t cpsr) {
    return (cpsr >> 28) & 1;
}
static inline void setOverflow(CortexMCore *cpu, bool f) {
    if (f)
        cpu->cpsr |= (1 << 28);
    else
        cpu->cpsr &= ~(1 << 28);
}

static inline void setNZ(CortexMCore *cpu, int32_t result) {
    setNeg(cpu, result < 0);
    setZero(cpu, result == 0);
}


/////////////////////////////////////////////////
//          Utilities
/////////////////////////////////////////////////


// Extend a W-bit wide two's complement value to 32-bit.
static inline int32_t signExtend(uint32_t value, unsigned w) {
    const uint32_t msb = 1 << (w - 1);
    if (value & msb) {
        const uint32_t upper = (uint32_t)-1 << w;
        value |= upper;
    }
    return value;
}

// Rotate right
static inline uint32_t ror(uint32_t a, uint32_t b) {
    if (b < 32)
        return (a >> b) | (a << (32 - b));
    return 0;
}

enum Conditions {
    EQ = 0,    // Equal
    NE = 1,    // Not Equal
    CS = 2,    // Carry Set
    CC = 3,    // Carry Clear
    MI = 4,    // Minus, Negative
    PL = 5,    // Plus, positive, or zero
    VS = 6,    // Overflow
    VC = 7,    // No overflow
    HI = 8,    // Unsigned higher
    LS = 9,    // Unsigned lower or same
    GE = 10,    // Signed greater than or equal
    LT = 11,    // Signed less than
    GT = 12,    // Signed greater than
    LE = 13,    // Signed less than or equal
    NoneAL = 14 // None, always, unconditional
};

static inline bool conditionPassed(uint8_t cond, reg_t cpsr)
{
    switch (cond) {
    case EQ: return  getZero(cpsr);
    case NE: return !getZero(cpsr);
    case CS: return  getCarry(cpsr);
    case CC: return !getCarry(cpsr);
    case MI: return  getNeg(cpsr);
    case PL: return !getNeg(cpsr);
    case VS: return  getOverflow(cpsr);
    case VC: return !getOverflow(cpsr);
    case HI: return  getCarry(cpsr) && !getZero(cpsr);
    case LS: return !getCarry(cpsr) || getZero(cpsr);
    case GE: return  getNeg(cpsr) == getOverflow(cpsr);
    case LT: return  getNeg(cpsr) != getOverflow(cpsr);
    case GT: return (getZero(cpsr) == 0) && (getNeg(cpsr) == getOverflow(cpsr));
    case LE: return (getZero(cpsr) == 1) || (getNeg(cpsr) != getOverflow(cpsr));
    case NoneAL: return true;
    default:
        assert(0 && "invalid condition code");
        return false;
    }
}

/////////////////////////////////////////////////
//          Branch Emulation Utilities
/////////////////////////////////////////////////

static inline reg_t branchTargetB(uint16_t instr, reg_t pc)
{
    // encoding T2 only
    unsigned imm11 = instr & 0x7FF;
    return calculateBranchOffset(pc, signExtend(imm11 << 1, 12));
}

static inline reg_t passedBranchTargetCondB(uint16_t instr, reg_t pc)
{
    unsigned imm8 = instr & 0xff;
    return calculateBranchOffset(pc, signExtend(imm8 << 1, 9));
}

static inline reg_t branchTargetCondB(uint16_t instr, reg_t pc, reg_t cpsr)
{
    unsigned cond = (instr >> 8) & 0xf;

    if (conditionPassed(cond, cpsr))
        return passedBranchTargetCondB(instr, pc);
    else
        return pc;
}

static inline reg_t passedBranchTargetCBZ_CBNZ(uint16_t instr, reg_t pc)
{
    unsigned i = instr & (1 << 9);
    unsigned imm5 = (instr >> 3) & 0x1f;

    // ZeroExtend(i:imm5:'0')
    return calculateBranchOffset(pc, (i << 6) | (imm5 << 1));
}

static inline reg_t branchTargetCBZ_CBNZ(uint16_t instr, reg_t pc, reg_t cpsr, reg_t Rn)
{
    bool nonzero = instr & (1 << 11);

    if (nonzero ^ (Rn == 0))
        return passedBranchTargetCBZ_CBNZ(instr, pc);
    else
        return pc;
}


/////////////////////////////////////////////////
//              Helper Ops
/////////////////////////////////////////////////


static inline reg_t opLSL(CortexMCore *cpu, reg_t a, reg_t b) {
    // Note: Intentionally truncates to 32-bit
    setCarry(cpu, b ? ((0x80000000 >> (b - 1)) & a) != 0 : 0);
    uint32_t result = b < 32 ? a << b : 0;
    setNZ(cpu, result);
    return result;
}

static inline reg_t opLSR(CortexMCore *cpu, reg_t a, reg_t b) {
    // Note: Intentionally truncates to 32-bit
    setCarry(cpu, b ? ((1 << (b - 1)) & a) != 0 : 0);
    uint32_t result = (b < 32) ? (a >> b) : 0;
    setNZ(cpu, result);
    return result;
}

static inline reg_t opASR(CortexMCore *cpu, reg_t a, reg_t b) {
    // Note: Intentionally truncates to 32-bit
    setCarry(cpu, b ? ((1 << (b - 1)) & a) != 0 : 0);
    uint32_t result = (b < 32) ? ((int32_t)a >> b) : 0;
    setNZ(cpu, result);
    return result;
}

static inline reg_t opADD(CortexMCore *cpu, reg_t a, reg_t b, reg_t carry) {
    // Based on AddWithCarry() in the ARMv7 ARM, page A2-8
    uint64_t uSum32 = (uint64_t)(uint32_t)a + (uint32_t)b + (uint32_t)carry;
    int64_t sSum32 = (int64_t)(int32_t)a + (int32_t)b + (uint32_t)carry;
    setNZ(cpu, sSum32);
    setOverflow(cpu, (int32_t)sSum32 != sSum32);
    setCarry(cpu, (uint32_t)uSum32 != uSum32);

    // Preserve full reg_t width in result, even though we use 32-bit value for flags
    return a + b + carry;
}

static inline reg_t opAND(CortexMCore *cpu, reg_t a, reg_t b) {
    reg_t result = a & b;
    setNZ(cpu, result);
    return result;
}

static inline reg_t opEOR(CortexMCore *cpu, reg_t a, reg_t b) {
    reg_t result = a ^ b;
    setNZ(cpu, result);
    return result;
}


/////////////////////////////////////////////////
//          Arithmetic Instructions
/////////////////////////////////////////////////


void emulateLSLImm(CortexMCore *cpu, uint16_t inst)
{
    unsigned imm5 = (inst >> 6) & 0x1f;
    unsigned Rm = (inst >> 3) & 0x7;
    unsigned Rd = inst & 0x7;

    cpu->regs[Rd] = opLSL(cpu, cpu->regs[Rm], imm5);
}

void emulateLSRImm(CortexMCore *cpu, uint16_t inst)
{
    unsigned imm5 = (inst >> 6) & 0x1f;
    unsigned Rm = (inst >> 3) & 0x7;
    unsigned Rd = inst & 0x7;

    if (imm5 == 0)
        imm5 = 32;

    cpu->regs[Rd] = opLSR(cpu, cpu->regs[Rm], imm5);
}

void emulateASRImm(CortexMCore *cpu, uint16_t instr)
{
    unsigned imm5 = (instr >> 6) & 0x1f;
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    if (imm5 == 0)
        imm5 = 32;

    cpu->regs[Rd] = opASR(cpu, cpu->regs[Rm], imm5);
}

void emulateADDReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = opADD(cpu, cpu->regs[Rn], cpu->regs[Rm], 0);
}

void emulateSUBReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = opADD(cpu, cpu->regs[Rn], ~cpu->regs[Rm], 1);
}

void emulateADD3Imm(CortexMCore *cpu, uint16_t instr)
{
    reg_t imm3 = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = opADD(cpu, cpu->regs[Rn], imm3, 0);
}

void emulateSUB3Imm(CortexMCore *cpu, uint16_t instr)
{
    reg_t imm3 = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = opADD(cpu, cpu->regs[Rn], ~imm3, 1);
}

void emulateMovImm(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rd = (instr >> 8) & 0x7;
    unsigned imm8 = instr & 0xff;

    cpu->regs[Rd] = imm8;
}

void emulateCmpImm(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rn = (instr >> 8) & 0x7;
    reg_t imm8 = instr & 0xff;

    // ignore the result, just interested in side effects
    opADD(cpu, cpu->regs[Rn], ~imm8, 1);
}

void emulateADD8Imm(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rdn = (instr >> 8) & 0x7;
    reg_t imm8 = instr & 0xff;

    cpu->regs[Rdn] = opADD(cpu, cpu->regs[Rdn], imm8, 0);
}

void emulateSUB8Imm(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rdn = (instr >> 8) & 0x7;
    reg_t imm8 = instr & 0xff;

    cpu->regs[Rdn] = opADD(cpu, cpu->regs[Rdn], ~imm8, 1);
}


/////////////////////////////////////////////////
//              Data Processing
/////////////////////////////////////////////////


void emulateANDReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = opAND(cpu, cpu->regs[Rdn], cpu->regs[Rm]);
}

void emulateEORReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = opEOR(cpu, cpu->regs[Rdn], cpu->regs[Rm]);
}

void emulateLSLReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    unsigned shift = cpu->regs[Rm] & 0xff;
    cpu->regs[Rdn] = opLSL(cpu, cpu->regs[Rdn], shift);
}

void emulateLSRReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    unsigned shift = cpu->regs[Rm] & 0xff;
    cpu->regs[Rdn] = opLSR(cpu, cpu->regs[Rdn], shift);
}

void emulateASRReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    unsigned shift = cpu->regs[Rm] & 0xff;
    cpu->regs[Rdn] = opASR(cpu, cpu->regs[Rdn], shift);
}

void emulateADCReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = opADD(cpu, cpu->regs[Rdn], cpu->regs[Rm], getCarry(cpu->cpsr));
}

void emulateSBCReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = opADD(cpu, cpu->regs[Rdn], ~cpu->regs[Rm], getCarry(cpu->cpsr));
}

void emulateRORReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = ror(cpu->regs[Rdn], cpu->regs[Rm]);
}

void emulateTSTReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    opAND(cpu, cpu->regs[Rdn], cpu->regs[Rm]);
}

void emulateRSBImm(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = opADD(cpu, ~cpu->regs[Rn], 0, 1);
}

void emulateCMPReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    opADD(cpu, cpu->regs[Rdn], ~cpu->regs[Rm], 1);
}

void emulateCMNReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    opADD(cpu, cpu->regs[Rdn], cpu->regs[Rm], 0);
}

void emulateORRReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    reg_t result = cpu->regs[Rdn] | cpu->regs[Rm];
    cpu->regs[Rdn] = result;
    setNZ(cpu, result);
}

void emulateMUL(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    uint64_t result = (uint64_t)cpu->regs[Rdn] * (uint64_t)cpu->regs[Rm];
    cpu->regs[Rdn] = (uint32_t) result;

    // Flag calculations always use the full 64-bit result
    setNeg(cpu, (int64_t)result < 0);
    setZero(cpu, result == 0);
}

void emulateBICReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = (uint32_t) (cpu->regs[Rdn] & ~(cpu->regs[Rm]));
}

void emulateMVNReg(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = (uint32_t) ~cpu->regs[Rm];
}


/////////////////////////////////////////////////
//              Misc Instructions
/////////////////////////////////////////////////


void emulateSXTH(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = (uint32_t) signExtend(cpu->regs[Rm], 16);
}

void emulateSXTB(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = (uint32_t) signExtend(cpu->regs[Rm], 8);
}

void emulateUXTH(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = cpu->regs[Rm] & 0xFFFF;
}

void emulateUXTB(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0x7;
    unsigned Rdn = instr & 0x7;

    cpu->regs[Rdn] = cpu->regs[Rm] & 0xFF;
}

void emulateMOV(CortexMCore *cpu, uint16_t instr)
{
    // Thumb T5 encoding, does not affect flags.
    // This subset does not support high register access.

    unsigned Rs = (instr >> 3) & 0x7;
    unsigned Rd = instr & 0x7;

    cpu->regs[Rd] = cpu->regs[Rs];
}


/////////////////////////////////////////////////
//          Branching Instructions
/////////////////////////////////////////////////


void emulateB(CortexMCore *cpu, uint16_t instr)
{
    reg_t oldPC = cpu->pc;
    reg_t newPC = branchTargetB(instr, oldPC);

    if (newPC != oldPC) {
        cpu->pc = newPC;
        // XXX: calculate cycles
    }
}


void emulateCondB(CortexMCore *cpu, uint16_t instr)
{
    reg_t oldPC = cpu->pc;
    reg_t newPC = branchTargetCondB(instr, oldPC, cpu->cpsr);

    if (newPC != oldPC) {
        cpu->pc = newPC;
        // XXX: calculate cycles
    }
}

void emulateBL(CortexMCore *cpu, uint32_t instr)
{
    reg_t oldPC = cpu->pc;
    cpu->lr = oldPC | 0x1;

    uint32_t imm10 = (instr >> 16) & 0x1f;
    uint32_t imm11 = instr & 0x3f;

    uint32_t S  = (instr >> 26) & 0x1;
    uint32_t J1 = (instr >> 13) & 0x1;
    uint32_t J2 = (instr >> 11) & 0x1;

    uint32_t I1 = (J1 ^ S) ? 0 : 1;     // NOT(J1 EOR S)
    uint32_t I2 = (J2 ^ S) ? 0: 1;      // NOT(J2 EOR S)

    uint32_t imm32 = signExtend((S << 24) |
                                (I1 << 23) |
                                (I2 << 22) |
                                (imm10 << 12) |
                                (imm11 << 1), 32);

    reg_t newPC = cpu->pc + imm32;

    if (newPC != oldPC) {
        cpu->pc = newPC;
        // XXX: calculate cycles
    }
}

void emulateBLX(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0xf;
    if (Rm == 15) {
        assert(0 && "illegal value in BLX instruction");
        // emulateFault()
    }

    cpu->lr = (cpu->pc - sizeof(uint16_t)) | 0x1;
    cpu->pc = cpu->regs[Rm];   // BLXWritePC(target);
}

void emulateBX(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rm = (instr >> 3) & 0xf;
    if (Rm == 15) {
        assert(0 && "illegal value in BLX instruction");
        // emulateFault()
    }

    // only Thumb addresses are supported
    uint32_t target = cpu->regs[Rm];
    if (target & 0x1) {
        assert(0 && "illegal target in BX instruction");
        // emulateFault()
    }
    cpu->pc = target;
}

void emulateCBZ_CBNZ(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rn = instr & 0x7;
    reg_t oldPC = cpu->pc;
    reg_t newPC = branchTargetCBZ_CBNZ(instr, oldPC, cpu->cpsr, cpu->regs[Rn]);

    if (newPC != oldPC) {
        cpu->pc = newPC;
        // XXX: calculate cycles
    }
}


/////////////////////////////////////////////////
//              Memory Instructions
/////////////////////////////////////////////////


void emulateSTRReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     * need T2 to support armv7m
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;  // XXX: overflow?
    uint32_t data = cpu->regs[Rt];

    cmWrite32(cpu, addr, data);
}

void emulateSTRHReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     * need T2 to support armv7m
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;  // XXX: overflow?
    uint16_t data = cpu->regs[Rt] & 0xffff;

    cmWrite16(cpu, addr, data);
}

void emulateSTRBReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     * need T2 to support armv7m
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;  // XXX: overflow?
    uint8_t data = cpu->regs[Rt] & 0xff;

    cmWrite8(cpu, addr, data);
}

void emulateLDRSBReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;

    cpu->regs[Rt] = signExtend(cmRead8(cpu, addr), 32);
}

void emulateLDRReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;

    cpu->regs[Rt] = cmRead32(cpu, addr);
}

void emulateLDRHReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;

    cpu->regs[Rt] = cmRead16(cpu, addr) & 0xffff;
}

void emulateLDRBReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;

    cpu->regs[Rt] = cmRead8(cpu, addr) & 0xff;
}

void emulateLDRSHReg(CortexMCore *cpu, uint16_t instr)
{
    /*
     * encoding T1 only
     */

    unsigned Rm = (instr >> 6) & 0x7;
    unsigned Rn = (instr >> 3) & 0x7;
    unsigned Rt = instr & 0x7;

    uint32_t offset = cpu->regs[Rm];
    uint32_t addr = cpu->regs[Rn] + offset;

    cpu->regs[Rt] = signExtend(cmRead16(cpu, addr), 32);
}

void emulateSTRSPImm(CortexMCore *cpu, uint16_t instr)
{
    // encoding T2 only
    unsigned Rt = (instr >> 8) & 0x7;
    unsigned imm8 = instr & 0xff;
    reg_t addr = cpu->sp + (imm8 << 2);

    cmWrite32(cpu, addr, cpu->regs[Rt]);

    // XXX: cycle count
}

void emulateLDRSPImm(CortexMCore *cpu, uint16_t instr)
{
    // encoding T2 only
    unsigned Rt = (instr >> 8) & 0x7;
    unsigned imm8 = instr & 0xff;
    reg_t addr = cpu->sp + (imm8 << 2);
    cpu->regs[Rt] = cmRead32(cpu, addr);

    // XXX: cycle count
}

void emulateADDSpImm(CortexMCore *cpu, uint16_t instr)
{
    // encoding T1 only
    unsigned Rd = (instr >> 8) & 0x7;
    unsigned imm8 = instr & 0xff;

    /*
     * NB: We need to squash SP here on 64-bit systems, in order to keep
     *     usermode stack pointers consistent. On real hardware, addresses
     *     taken from the stack will always be 32-bit physical addresses,
     *     whereas addresses formed in other ways will be 32-bit virtual
     *     addresses. This is fine, since either form is valid, and there's
     *     no guarantee that pointer math between stack and heap addresses
     *     will produce a meaningful result.
     *
     *     However! On 64-bit, with our pointer-squashing, we need to worry
     *     about the difference between real 64-bit physical addresses, and
     *     addresses that have been squashed back to 32-bit. Normally stack
     *     addresses stay physical as they're copied from SP to r0-7, but that
     *     means that stack addresses which have been kept only in registers
     *     will differ from stack addresses that have ever made a trip into
     *     (32-bit) guest memory and have been squashed.
     *
     *     So, to keep all stack addresses consistent, we choose to use 32-bit
     *     virtual addresses everywhere when we're on 64-bit hosts. On 32-bit
     *     hosts, the squash will have no effect and we'll still be using
     *     32-bit physical addresses.
     */

//    cpu->regs[Rd] = SvmMemory::squashPhysicalAddr(cpu->sp) + (imm8 << 2);
    cpu->regs[Rd] = cpu->sp + (imm8 << 2);
}

void emulateLDRLitPool(CortexMCore *cpu, uint16_t instr)
{
    unsigned Rt = (instr >> 8) & 0x7;
    unsigned imm8 = instr & 0xFF;

    // Round up to the next 32-bit boundary
    reg_t addr = ((cpu->pc + 3) & ~3) + (imm8 << 2);
    cpu->regs[Rt] = cmRead32(cpu, addr);

    // XXX: cycle count
}


/////////////////////////////////////////////////
//              32-bit instructions
/////////////////////////////////////////////////

void emulateSTR(CortexMCore *cpu, uint32_t instr)
{
    unsigned imm12 = instr & 0xFFF;
    unsigned Rn = (instr >> 16) & 0xF;
    unsigned Rt = (instr >> 12) & 0xF;
    reg_t addr = cpu->regs[Rn] + imm12;

    cmWrite32(cpu, addr, cpu->regs[Rt]);

    // XXX: count cycles
}

void emulateLDR(CortexMCore *cpu, uint32_t instr)
{
    unsigned imm12 = instr & 0xFFF;
    unsigned Rn = (instr >> 16) & 0xF;
    unsigned Rt = (instr >> 12) & 0xF;
    reg_t addr = cpu->regs[Rn] + imm12;

    cpu->regs[Rt] = cmRead32(cpu, addr);

    // XXX: count cycles
}

void emulateSTRBH(CortexMCore *cpu, uint32_t instr)
{
    const unsigned HalfwordBit = 1 << 21;

    unsigned imm12 = instr & 0xFFF;
    unsigned Rn = (instr >> 16) & 0xF;
    unsigned Rt = (instr >> 12) & 0xF;
    reg_t addr = cpu->regs[Rn] + imm12;

    if (instr & HalfwordBit) {
        cmWrite16(cpu, addr, cpu->regs[Rt]);
    } else {
        cmWrite8(cpu, addr, cpu->regs[Rt]);
    }

    // XXX: count cycles
}

void emulateLDRBH(CortexMCore *cpu, uint32_t instr)
{
#define kHalfwordBit (1 << 21)
#define kSignExtBit  (1 << 24)

    unsigned imm12 = instr & 0xFFF;
    unsigned Rn = (instr >> 16) & 0xF;
    unsigned Rt = (instr >> 12) & 0xF;
    reg_t addr = cpu->regs[Rn] + imm12;

    switch (instr & (kHalfwordBit | kSignExtBit)) {
    case 0:
        cpu->regs[Rt] = cmRead8(cpu, addr);
        break;

    case kHalfwordBit:
        cpu->regs[Rt] = cmRead16(cpu, addr);
        break;

    case kSignExtBit:
        cpu->regs[Rt] = (uint32_t)signExtend(cmRead8(cpu, addr), 8);
        break;

    case (kHalfwordBit | kSignExtBit):
        cpu->regs[Rt] = (uint32_t)signExtend(cmRead16(cpu, addr), 16);
        break;
    }

    // XXX: count cycles
}

void emulateMOVWT(CortexMCore *cpu, uint32_t instr)
{
    const unsigned TopBit = 1 << 23;

    unsigned Rd = (instr >> 8) & 0xF;
    unsigned imm16 =
        (instr & 0x000000FF) |
        (instr & 0x00007000) >> 4 |
        (instr & 0x04000000) >> 15 |
        (instr & 0x000F0000) >> 4;

    if (TopBit & instr) {
        cpu->regs[Rd] = (cpu->regs[Rd] & 0xFFFF) | (imm16 << 16);
    } else {
        cpu->regs[Rd] = imm16;
    }
}

void emulateDIV(CortexMCore *cpu, uint32_t instr)
{
    const unsigned UnsignedBit = 1 << 21;

    unsigned Rn = (instr >> 16) & 0xF;
    unsigned Rd = (instr >> 8) & 0xF;
    unsigned Rm = instr & 0xF;

    uint32_t m32 = (uint32_t)cpu->regs[Rm];

    if (m32 == 0) {
        cpu->regs[Rd] = 0; // Divide by zero, defined to return 0
    } else if (UnsignedBit & instr) {
        cpu->regs[Rd] = (uint32_t)cpu->regs[Rn] / m32;
    } else {
        cpu->regs[Rd] = (int32_t)cpu->regs[Rn] / (int32_t)m32;
    }

    // XXX: count cycles
}

void emulateCLZ(CortexMCore *cpu, uint32_t instr)
{
    unsigned Rm1 = (instr >> 16) & 0xF;
    unsigned Rd  = (instr >> 8) & 0xF;
    unsigned Rm2 = instr & 0xF;

    // ARM ARM states that the two Rm fields must be consistent
    if (Rm1 != Rm2) {
        assert(false && "simulator error - incorrect data in CLZ instruction");
        // XXX: fault instead
    }
    uint32_t m32 = (uint32_t)cpu->regs[Rm1];

    // Note that GCC leaves __builtin_clz(0) undefined, whereas for ARM it's 32.
    if (m32 == 0)
        cpu->regs[Rd] = 32;
    else
        cpu->regs[Rd] = __builtin_clz(m32);
}
