#ifndef CORTEX_M_CORE_H
#define CORTEX_M_CORE_H

#include "memory.h"

#ifdef __cplusplus
extern "C" {
#endif

// Registers must be wide enough to hold a native pointer
typedef uintptr_t reg_t;

enum cmCoreType {
    CoreARMv6M,
    CoreARMv7M
};

enum cmCpuMode {
    ModeThread,
    ModeHandler
};

enum cmException {
    ExceptionReset          = 1,
    ExceptionNMI            = 2,
    ExceptionHardFault      = 3,
    // 4-10 reserved
    ExceptionSVCall         = 11,
    // 12-13 reserved
    ExceptionPendSV         = 14,
    ExceptionSysTick        = 15,
    ExceptionExternalBase   = 16
//    External Interrupt(N) = ExceptionExternalBase + N
};

// elements common to Cortex-M 0,3,4
typedef struct CortexMCore_t
{
    const struct CortexMVTable *vtable;
    const char *name;
    enum cmCoreType coreType;

    union {
        reg_t regs[16];
        struct {
            reg_t r0;
            reg_t r1;
            reg_t r2;
            reg_t r3;
            reg_t r4;
            reg_t r5;
            reg_t r6;
            reg_t r7;
            reg_t r8;
            reg_t r9;
            reg_t r10;
            reg_t r11;
            reg_t r12;
            reg_t sp;
            reg_t lr;
            reg_t pc;
            reg_t cpsr;
        };
    };

    enum cmCpuMode mode;

    // XXX: better way to represent components?
    struct Memory *flash;
    struct Memory *ram;

    // XXX: better way to group control registers?
    uint32_t VTOR;      // vector table offset, [31:7]
    uint32_t CONTROL;   // nPRIV bit[0], SPSEL, bit[1], bits[31:2] reserved

} CortexMCore;

struct CortexMVTable
{
    // memory access
    uint8_t  (*read8)(CortexMCore *cpu, uint32_t addr);
    uint16_t (*read16)(CortexMCore *cpu, uint32_t addr);
    uint32_t (*read32)(CortexMCore *cpu, uint32_t addr);

    void (*write8)(CortexMCore *cpu, uint32_t addr, uint8_t data);
    void (*write16)(CortexMCore *cpu, uint32_t addr, uint16_t data);
    void (*write32)(CortexMCore *cpu, uint32_t addr, uint32_t data);

    // instruction dispatch
    void (*execute16)(CortexMCore *cpu, uint16_t instr);
    void (*execute32)(CortexMCore *cpu, uint32_t instr);
};

#ifdef __cplusplus
} // extern "C"
#endif

#endif // CORTEX_M_CORE_H
