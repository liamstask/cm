#include "armv7m.h"
#include "armv7m-mem.h"
#include "thumb2-emu.h"
#include "thumb-encoding.h"

#include <stdio.h>
#include <assert.h>

static void armv7mExecute16(CortexMCore *cpu, uint16_t instr);
static void armv7mExecute32(CortexMCore *cpu, uint32_t instr);

static const struct CortexMVTable ARMv7M_vtable = {
    armv7mRead8,
    armv7mRead16,
    armv7mRead32,
    armv7mWrite8,
    armv7mWrite16,
    armv7mWrite32,
    armv7mExecute16,
    armv7mExecute32
};

void cmInitARMv7M(CortexMCore *cpu)
{
    cpu->vtable = &ARMv7M_vtable;
    cpu->name = "armv7-m";
    cpu->coreType = CoreARMv7M;
}

/////////////////////////////////////////////////
//          Instruction Dispatch
/////////////////////////////////////////////////


void armv7mExecute16(CortexMCore *cpu, uint16_t instr)
{
    if ((instr & AluMask) == AluTest) {
        // lsl, lsr, asr, add, sub, mov, cmp
        // take bits [13:11] to group these
        uint8_t prefix = (instr >> 11) & 0x7;
        switch (prefix) {
        case 0: emulateLSLImm(cpu, instr); return;
        case 1: emulateLSRImm(cpu, instr); return;
        case 2: emulateASRImm(cpu, instr); return;
        case 3: {
            uint8_t subop = (instr >> 9) & 0x3;
            switch (subop) {
            case 0: emulateADDReg(cpu, instr);  return;
            case 1: emulateSUBReg(cpu, instr);  return;
            case 2: emulateADD3Imm(cpu, instr); return;
            case 3: emulateADD8Imm(cpu, instr); return;
            }
        }
        case 4: emulateMovImm(cpu, instr);  return;
        case 5: emulateCmpImm(cpu, instr);  return;
        case 6: emulateADD8Imm(cpu, instr); return;
        case 7: emulateSUB8Imm(cpu, instr); return;
        }
        assert(0 && "unhandled ALU instruction!");
    }

    if ((instr & DataProcMask) == DataProcTest) {
        uint8_t opcode = (instr >> 6) & 0xf;
        switch (opcode) {
        case 0:  emulateANDReg(cpu, instr); return;
        case 1:  emulateEORReg(cpu, instr); return;
        case 2:  emulateLSLReg(cpu, instr); return;
        case 3:  emulateLSRReg(cpu, instr); return;
        case 4:  emulateASRReg(cpu, instr); return;
        case 5:  emulateADCReg(cpu, instr); return;
        case 6:  emulateSBCReg(cpu, instr); return;
        case 7:  emulateRORReg(cpu, instr); return;
        case 8:  emulateTSTReg(cpu, instr); return;
        case 9:  emulateRSBImm(cpu, instr); return;
        case 10: emulateCMPReg(cpu, instr); return;
        case 11: emulateCMNReg(cpu, instr); return;
        case 12: emulateORRReg(cpu, instr); return;
        case 13: emulateMUL(cpu, instr);    return;
        case 14: emulateBICReg(cpu, instr); return;
        case 15: emulateMVNReg(cpu, instr); return;
        }
    }

    if ((instr & MiscMask) == MiscTest) {
        uint8_t opcode = (instr >> 5) & 0x7f;
        if ((opcode & 0x78) == 0x2) {   // bits [6:3] of opcode identify this group
            switch (opcode & 0x6) {     // bits [2:1] of the opcode identify the instr
            case 0: emulateSXTH(cpu, instr); return;
            case 1: emulateSXTB(cpu, instr); return;
            case 2: emulateUXTH(cpu, instr); return;
            case 3: emulateUXTB(cpu, instr); return;
            }
        }
    }

    if ((instr & SvcMask) == SvcTest) {
//        emulateSVC(cpu, instr);
        return;
    }

    if ((instr & PcRelLdrMask) == PcRelLdrTest) {
        emulateLDRLitPool(cpu, instr); return;
    }

    if ((instr & SpRelLdrStrMask) == SpRelLdrStrTest) {
        uint16_t isLoad = instr & (1 << 11);
        if (isLoad) emulateLDRSPImm(cpu, instr);
        else        emulateSTRSPImm(cpu, instr);
        return;
    }

    if ((instr & SpRelAddMask) == SpRelAddTest) {
        emulateADDSpImm(cpu, instr); return;
    }

    if ((instr & UncondBranchMask) == UncondBranchTest) {
        emulateB(cpu, instr); return;
    }

    if ((instr & CompareBranchMask) == CompareBranchTest) {
        emulateCBZ_CBNZ(cpu, instr); return;
    }

    if ((instr & CondBranchMask) == CondBranchTest) {
        emulateCondB(cpu, instr); return;
    }

    if (instr == Nop) {
        return;
    }

    fprintf(stderr, "CM: unhandled 16bit instruction: 0x%x\n", instr);
    assert(false);
    // emulateFault();
}

void armv7mExecute32(CortexMCore *cpu, uint32_t instr)
{
    if ((instr & StrMask) == StrTest) {
        emulateSTR(cpu, instr); return;
    }

    if ((instr & StrBhMask) == StrBhTest) {
        emulateSTRBH(cpu, instr); return;
    }

    if ((instr & LdrBhMask) == LdrBhTest) {
        emulateLDRBH(cpu, instr); return;
    }

    if ((instr & LdrMask) == LdrTest) {
        emulateLDR(cpu, instr); return;
    }

    if ((instr & MovWtMask) == MovWtTest) {
        emulateMOVWT(cpu, instr); return;
    }

    if ((instr & DivMask) == DivTest) {
        emulateDIV(cpu, instr); return;
    }

    if ((instr & ClzMask) == ClzTest) {
        emulateCLZ(cpu, instr); return;
    }

    fprintf(stderr, "CM: unhandled 32bit instruction: 0x%x\n", instr);
    assert(false);
    // emulateFault();
}

