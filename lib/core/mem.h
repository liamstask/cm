#ifndef MEM_H
#define MEM_H

#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif


//////////////////////////////////////////////////
//              memory access wrappers
//////////////////////////////////////////////////


static inline uint8_t cmRead8(CortexMCore *cpu, uint32_t addr) {
    return cpu->vtable->read8(cpu, addr);
}

static inline uint16_t cmRead16(CortexMCore *cpu, uint32_t addr) {
    return cpu->vtable->read16(cpu, addr);
}

static inline uint32_t cmRead32(CortexMCore *cpu, uint32_t addr) {
    return cpu->vtable->read32(cpu, addr);
}

static inline void cmWrite8(CortexMCore *cpu, uint32_t addr, uint8_t data) {
    return cpu->vtable->write8(cpu, addr, data);
}

static inline void cmWrite16(CortexMCore *cpu, uint32_t addr, uint16_t data) {
    return cpu->vtable->write16(cpu, addr, data);
}

static inline void cmWrite32(CortexMCore *cpu, uint32_t addr, uint32_t data) {
    return cpu->vtable->write32(cpu, addr, data);
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif // MEM_H
