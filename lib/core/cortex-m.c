#include "cortex-m.h"
#include "mem.h"
#include "thumb2-emu.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>

bool cmReset(CortexMCore *cpu)
{
    memset(cpu->ram->bytes, 0xff, cpu->ram->capacity);

    cpu->mode   = ModeThread;
    cpu->cpsr   = 0;

    // XXX: does this get updated here?
    cpu->VTOR = 0;

    // upon reset, initial stack pointer value is at 0x0
    // and pc can be found at 0x4.

    cpu->sp = cmRead32(cpu, 0x00000000);
    cpu->lr = 0xFFFFFFFF;
    cpu->pc = calculateBranchOffset(cmRead32(cpu, 0x00000004), 0);

    printf("reset - sp: 0x%08lx, lr: 0x%08lx, pc: 0x%08lx\n", cpu->sp, cpu->lr, cpu->pc);

    return true;
}

static uint16_t fetch(CortexMCore *cpu)
{
    /*
     * Fetch the current instruction,
     * and step to the next one.
     */

    uint16_t instr = cmRead16(cpu, cpu->pc);
    cpu->pc += sizeof(uint16_t);
    return instr;
}

void cmExecute(CortexMCore *cpu)
{
    /*
     * execute an instruction, may be 16 or 32 bit.
     */

    uint16_t instr = fetch(cpu);
    if (instructionSize(instr) == InstrBits16) {
        cpu->vtable->execute16(cpu, instr);
    } else {
        uint16_t instrLow = fetch(cpu);
        cpu->vtable->execute32(cpu, instr << 16 | instrLow);
    }
}


/////////////////////////////////////////////////
//          exception handling
//          section B1.5.6
/////////////////////////////////////////////////


void cmExceptionEntry(CortexMCore *cpu, unsigned exceptionType)
{
    cmPushStack(cpu, exceptionType);
    cmExceptionTaken(cpu, exceptionType);
}

static uint32_t cmReturnAddress(CortexMCore *cpu, unsigned exceptionType)
{
    if (exceptionType == ExceptionHardFault) {
        // the address of the instruction causing the fault
        return cpu->pc - sizeof(uint16_t);
    }

    // general case - return the address of the next instruction
    return cpu->pc;
}

void cmPushStack(CortexMCore *cpu, unsigned exceptionType)
{
    const uint32_t stackedContextSize = 0x20;

    // select the appropriate stack pointer... we only support a single sp for now
    uint32_t frameptr = cpu->sp - stackedContextSize;

#if 0
    if (CONTROL.SPSEL == '1' && cpu->mode == ModeThread) {
        frameptralign = SP_process<2>;
        SP_process = (SP_process - 0x20) AND NOT(ZeroExtend('100',32));
        frameptr = SP_process;
    } else {
        frameptralign = SP_main<2>;
        SP_main = (SP_main - 0x20) AND NOT(ZeroExtend('100',32));
        frameptr = SP_main;
    }
#endif

    cmWrite32(cpu, frameptr, cpu->r0);
    cmWrite32(cpu, frameptr + 0x04, cpu->r1);
    cmWrite32(cpu, frameptr + 0x08, cpu->r2);
    cmWrite32(cpu, frameptr + 0x0C, cpu->r3);
    cmWrite32(cpu, frameptr + 0x10, cpu->r12);
    cmWrite32(cpu, frameptr + 0x14, cpu->lr);
    cmWrite32(cpu, frameptr + 0x18, cmReturnAddress(cpu, exceptionType));
    cmWrite32(cpu, frameptr + 0x1C, cpu->cpsr); // XXX: should be xPSR<31:10>:frameptralign:xPSR<8:0>

    if (cpu->mode == ModeHandler) {
        cpu->lr = 0xFFFFFFF1;
    } else {
        if (cpu->CONTROL & (1 << 1)) {
            cpu->lr = 0xFFFFFFFD;
        } else {
            cpu->lr = 0xFFFFFFF9;
        }
    }
}

void cmExceptionTaken(CortexMCore *cpu, unsigned exceptionNumber)
{
    const uint32_t UNKNOWN = 0xffffffff;

    unsigned i;
    for (i = 0; i < 3; i++) {
        cpu->regs[i] = UNKNOWN;
    }
    cpu->regs[12] = UNKNOWN;
    // apsr = UNKNOWN;
    cpu->mode = ModeHandler;
    cpu->cpsr = exceptionNumber;    // XXX: should be just [5:0]
//    SCS_UpdateStatusRegs();
//    SetEventRegister();
//    InstructionSynchronizationBarrier();

    uint32_t vectortable = cpu->VTOR >> 7;
    uint32_t start = cmRead32(cpu, vectortable + 4 * exceptionNumber);
    (void)start;
//    BLXWritePC(start);
}

void cmExceptionReturn(CortexMCore *cpu, uint32_t exc_return)
{
    assert(cpu->mode == ModeHandler);
    if ((exc_return & 0xffffff0) != 0xffffff0) {
//        then UNPREDICTABLE;
        assert(false && "bad EXC_RETURN value");
    }

//    int returningExceptionNumber = cpu->cpsr & 0x1f;

    uint32_t frameptr;

    int NestedActivation = 1;
//    int NestedActivation = ExceptionActiveBitCount(); // Number of active exceptions
//    if (ExceptionActive[returningExceptionNumber] == 0) {
//        assert(0 && "bad state in return from exception: UNPREDICTABLE");
//    }

    switch (exc_return & 0xf) {
    case 0x1:
        if (NestedActivation == 1) {
            assert(0 && "bad state in return from exception: UNPREDICTABLE");
        } else {
            frameptr = cpu->sp; // SP_main
            cpu->mode = ModeHandler;
            cpu->CONTROL &= ~(1 << 1);  // SPSEL = 0
        }
        break;

    case 0x9:
        if (NestedActivation != 1) {
            assert(0 && "bad state in return from exception: UNPREDICTABLE");
        } else {
            frameptr = cpu->sp; // SP_main
            cpu->mode = ModeThread;
            cpu->CONTROL &= ~(1 << 1);  // SPSEL = 0
        }
        break;

    case 0xd:
        if (NestedActivation != 1) {
            assert(0 && "bad state in return from exception: UNPREDICTABLE");
        } else {
            frameptr = cpu->sp; // SP_process
            cpu->mode = ModeThread;
            cpu->CONTROL |= (1 << 1);  // SPSEL = 1
        }
        break;

    default:
        assert(0 && "bad state in return from exception: UNPREDICTABLE");
    }

    cmDeactivate(cpu, 0);
    cmPopStack(cpu, frameptr, exc_return);

    if (cpu->mode == ModeHandler) {
        if ((cpu->cpsr & 0x1f) == 0) {
            assert(false && "bad status register in return from exception: UNPREDICTABLE");
        }
    } else {
        if ((cpu->cpsr & 0x1f) != 0) {
            assert(false && "bad status register in return from exception: UNPREDICTABLE");
        }
    }

//    SetEventRegister();
//    InstructionSynchronizationBarrier();

}

void cmDeactivate(CortexMCore *cpu, unsigned returningExceptionNumber)
{
//    ExceptionActive[returningExceptionNumber] = 0; // PRIMASK unchanged on exception exit
    return;
}

void cmPopStack(CortexMCore *cpu, uint32_t frameptr, uint32_t exc_return)
{
    /*
     * We're return from an exception.
     *
     * Pop the stored context from the stack, and prepare to return
     * either to another, deferred exception or normal execution.
     */

    cpu->r0   = cmRead32(cpu, frameptr);
    cpu->r1   = cmRead32(cpu, frameptr + 0x04);
    cpu->r2   = cmRead32(cpu, frameptr + 0x08);
    cpu->r3   = cmRead32(cpu, frameptr + 0x0C);
    cpu->r12  = cmRead32(cpu, frameptr + 0x10);
    cpu->lr   = cmRead32(cpu, frameptr + 0x14);
    cpu->pc   = cmRead32(cpu, frameptr + 0x18);
    cpu->cpsr = cmRead32(cpu, frameptr + 0x1C);

    // update SP
    switch (exc_return & 0xf) {
    case 0x1:
        break;

    case 0x9:
        break;

    case 0xd:
        break;
    }

    // XXX: status reg updates
}
