#ifndef ARMV6MMEM_H
#define ARMV6MMEM_H

#include <stdint.h>
#include <stdbool.h>

#include "armv6m.h"

// section B2.2.1
enum cmMemType {
    MemTypeNormal,
    MemTypeDevice,
    MemTypeStronglyOrdered
};

typedef struct cmMemoryAttributes_t {
    enum cmMemType type;
    // inner and outer are each 2-bits... reorg these into a single byte if needed
    uint8_t innerattrs; // '00' = Non-cacheable; '01' = WBWA; '10' = WT; '11' = WBnWA
    uint8_t outerattrs; // '00' = Non-cacheable; '01' = WBWA; '10' = WT; '11' = WBnWA
    bool shareable;
} cmMemoryAttributes;

typedef struct cmAddressDescriptor_t {
    cmMemoryAttributes memAttrs;
    uint32_t physAddr;
} cmAddressDescriptor;


/////////////////////////////////////////////////
//                  public api
/////////////////////////////////////////////////

void cmValidateAddress(cmAddressDescriptor *ad, uint32_t addr, bool ispriv, bool iswrite, bool isinstrfetch);
bool cmAddrIsAligned(uint32_t addr, unsigned size);

uint8_t  armv6mRead8(CortexMCore *cpu, uint32_t addr);
uint16_t armv6mRead16(CortexMCore *cpu, uint32_t addr);
uint32_t armv6mRead32(CortexMCore *cpu, uint32_t addr);

void armv6mWrite8(CortexMCore *cpu, uint32_t addr, uint8_t data);
void armv6mWrite16(CortexMCore *cpu, uint32_t addr, uint16_t data);
void armv6mWrite32(CortexMCore *cpu, uint32_t addr, uint32_t data);

#endif // ARMV6MMEM_H
