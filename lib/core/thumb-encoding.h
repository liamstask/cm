#ifndef THUMB_ENCODING_H
#define THUMB_ENCODING_H

/////////////////////////////////////////////////
//            Specific instructions
/////////////////////////////////////////////////

static const uint16_t BreakpointInstr   = 0xdfe8;       // 0b11011111 11101000
static const uint16_t Nop               = 0xbf00;       // 0b10111111 00000000

/////////////////////////////////////////////////
//      16-bit thumb instruction validators
/////////////////////////////////////////////////

static const uint16_t AluMask           = 0x3 << 14;    // 0b11xxxxxx xxxxxxxx
static const uint16_t AluTest           = 0x0;          // 0b00xxxxxx xxxxxxxx

static const uint16_t DataProcMask      = 0x3f << 10;   // 0b111111xx xxxxxxxx
static const uint16_t DataProcTest      = 0x10 << 10;   // 0b010000xx xxxxxxxx

static const uint16_t SpecialDataMask   = 0x3f << 10;   // 0b111111xx xxxxxxxx
static const uint16_t SpecialDataTest   = 0x11 << 10;   // 0b010001xx xxxxxxxx

static const uint16_t LoadStoreMask     = 0xf << 12;    // 0b1111xxxx xxxxxxxx
static const uint16_t LoadStoreTest     = 0x5 << 12;    // 0b0101xxxx xxxxxxxx

static const uint16_t LoadStore2Mask    = 0x7 << 13;    // 0b111xxxxx xxxxxxxx
static const uint16_t LoadStore2Test    = 0x3 << 13;    // 0b011xxxxx xxxxxxxx

static const uint16_t LoadStore3Mask    = LoadStore2Mask;
static const uint16_t LoadStore3Test    = 0x4 << 13;    // 0b100xxxxx xxxxxxxx

static const uint16_t MiscMask          = 0xff << 8;    // 0b11111111 xxxxxxxx
static const uint16_t MiscTest          = 0xb2 << 8;    // 0b10110010 xxxxxxxx

static const uint16_t SvcMask           = 0xff << 8;    // 0b11111111 xxxxxxxx
static const uint16_t SvcTest           = 0xdf << 8;    // 0b11011111 xxxxxxxx

static const uint16_t PcRelLdrMask      = 0x1f << 11;   // 0b11111xxx xxxxxxxx
static const uint16_t PcRelLdrTest      = 0x9 << 11;    // 0b01001xxx xxxxxxxx

static const uint16_t SpRelLdrStrMask   = 0xf << 12;    // 0b1111xxxx xxxxxxxx
static const uint16_t SpRelLdrStrTest   = 0x9 << 12;    // 0b1001xxxx xxxxxxxx

static const uint16_t SpRelAddMask      = 0x1f << 11;   // 0b11111xxx xxxxxxxx
static const uint16_t SpRelAddTest      = 0x15 << 11;   // 0b10101xxx xxxxxxxx

static const uint16_t UncondBranchMask  = 0x1f << 11;   // 0b11111xxx xxxxxxxx
static const uint16_t UncondBranchTest  = 0x1c << 11;   // 0b11100xxx xxxxxxxx

static const uint16_t CondBranchMask    = 0xf << 12;    // 0b1111xxxx xxxxxxxx
static const uint16_t CondBranchTest    = 0xd << 12;    // 0b1101xxxx xxxxxxxx

static const uint16_t CompareBranchMask = 0xf5 << 8;    // 0b11110101 xxxxxxxx
static const uint16_t CompareBranchTest = 0xb1 << 8;    // 0b1011x0x1 xxxxxxxx


/////////////////////////////////////////////////
//      32-bit thumb instruction validators
/////////////////////////////////////////////////

static const uint32_t StrMask   = 0x1ffff << 15;    // 0b11111111 11111111, 1xxxxxxx xxxxxxxx
static const uint32_t StrTest   = 0x1f192 << 15;    // 0b11111000 11001001, 0xxxxxxx xxxxxxxx

static const uint32_t StrBhMask = 0x1ffbf << 15;    // 0b11111111 11011111, 1xxxxxxx xxxxxxxx
static const uint32_t StrBhTest = 0x1f112 << 15;    // 0b11111000 10x01001, 0xxxxxxx xxxxxxxx

static const uint32_t LdrBhMask = 0x1fdbd << 15;    // 0b11111110 11011110, 1xxxxxxx xxxxxxxx
static const uint32_t LdrBhTest = 0x1f130 << 15;    // 0b1111100x 10x1100x, 0xxxxxxx xxxxxxxx

static const uint32_t LdrMask   = 0x1fffd << 15;    // 0b11111111 1111111x, 1xxxxxxx xxxxxxxx
static const uint32_t LdrTest   = 0x1f1b0 << 15;    // 0b11111000 1101100x, 0xxxxxxx xxxxxxxx

static const uint32_t MovWtMask = 0x1f6e11 << 11;   // 0b11111011 01110000, 10001xxx xxxxxxxx
static const uint32_t MovWtTest = 0x1e4800 << 11;   // 0b11110x10 x100xxxx, 0xxx0xxx xxxxxxxx

static const uint32_t DivMask   = 0xffd8f8f8;       // 0b11111111 11011000, 11111000 11111000
static const uint32_t DivTest   = 0xfb90f0f0;       // 0b11111011 10x10xxx, 11110xxx 11110xxx

static const uint32_t ClzMask   = 0xfffff8ff;       // 0b11111111 11111111, 11111000 11111111
static const uint32_t ClzTest   = 0xfab7f087;       // 0b11111010 10110111, 11110xxx 10000111

#endif // THUMB_ENCODING_H
