#include "armv7m-mem.h"

#include <assert.h>

uint8_t  armv7mRead8(CortexMCore *cpu, uint32_t addr)
{
    assert(0 && "unimplemented");
}

uint16_t armv7mRead16(CortexMCore *cpu, uint32_t addr)
{
    assert(0 && "unimplemented");
}

uint32_t armv7mRead32(CortexMCore *cpu, uint32_t addr)
{
    assert(0 && "unimplemented");
}

void armv7mWrite8(CortexMCore *cpu, uint32_t addr, uint8_t data)
{
    assert(0 && "unimplemented");
}

void armv7mWrite16(CortexMCore *cpu, uint32_t addr, uint16_t data)
{
    assert(0 && "unimplemented");
}

void armv7mWrite32(CortexMCore *cpu, uint32_t addr, uint32_t data)
{
    assert(0 && "unimplemented");
}
