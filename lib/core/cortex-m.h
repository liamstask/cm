#ifndef CORTEX_M_H
#define CORTEX_M_H

#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif


bool cmReset(CortexMCore *cpu);
void cmExecute(CortexMCore *cpu);

// XXX: which of these actually need to be public?
void cmExceptionEntry(CortexMCore *cpu, unsigned exceptionType);
void cmPushStack(CortexMCore *cpu, unsigned exceptionType);
void cmExceptionTaken(CortexMCore *cpu, unsigned exceptionNumber);
void cmDeactivate(CortexMCore *cpu, unsigned returningExceptionNumber);
void cmPopStack(CortexMCore *cpu, uint32_t frameptr, uint32_t exc_return);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // CORTEX_M_H
