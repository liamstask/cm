#ifndef ARMV6M_H
#define ARMV6M_H

#include <stdint.h>
#include <stdbool.h>

#include "memory.h"
#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif

void cmInitARMv6M(CortexMCore *cpu);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ARMV6M_H
