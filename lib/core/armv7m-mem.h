#ifndef ARMV7MMEM_H
#define ARMV7MMEM_H

#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif

uint8_t  armv7mRead8(CortexMCore *cpu, uint32_t addr);
uint16_t armv7mRead16(CortexMCore *cpu, uint32_t addr);
uint32_t armv7mRead32(CortexMCore *cpu, uint32_t addr);

void armv7mWrite8(CortexMCore *cpu, uint32_t addr, uint8_t data);
void armv7mWrite16(CortexMCore *cpu, uint32_t addr, uint16_t data);
void armv7mWrite32(CortexMCore *cpu, uint32_t addr, uint32_t data);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ARMV7MMEM_H
