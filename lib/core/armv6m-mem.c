#include "armv6m-mem.h"

#include <stdio.h>
#include <assert.h>

/*
 *                    System Address Map
 *
 * 0x00000000- 0x1FFFFFFF  Code         Typically ROM or flash memory
 * 0x20000000- 0x3FFFFFFF  SRAM         On-chip RAM
 * 0x40000000- 0x5FFFFFFF  Peripheral   On-chip peripheral memory map
 * 0x60000000- 0x7FFFFFFF  RAM          External RAM
 * 0x80000000- 0x9FFFFFFF  RAM          External RAM
 * 0xA0000000- 0xBFFFFFFF  Device       Shared device space
 * 0xC0000000- 0xDFFFFFFF  Device       Non-shared device space
 * 0xE0000000- 0xFFFFFFFF  System       For vendor system peripherals and PPB
 */

// section B3.5.1
void cmValidateAddress(cmAddressDescriptor *ad, uint32_t addr, bool ispriv, bool iswrite, bool isinstrfetch)
{
    /*
     * Validate addr, and populate ad based on the context
     * of the memory access.
     */

    ad->physAddr = addr;

    // XXX: do actual validation...

    //    ad->memAttrs = DefaultMemoryAttributes(address);

    //    Permissions perms;
    //    perms = DefaultPermissions(address);
}

bool cmModeIsPrivileged(CortexMCore *cpu)
{
    // XXX: implement
    return (cpu->mode == ModeHandler); // || CONTROL.nPRIV == '0');
}

bool cmAddrIsAligned(uint32_t addr, unsigned size)
{
    assert(size == 1 || size == 2 || size == 4);
    return (addr & (size - 1)) == 0;
}

static uintptr_t mem(CortexMCore *cpu, uint32_t addr)
{
    /*
     * Return the physical address of the memory
     * being referred to by the given virtual address.
     */

    switch (addr & 0xF0000000) {
    case 0x00000000:    // Flash
        assert(addr < cpu->flash->capacity); // XXX: fault instead
        return (uintptr_t)&cpu->flash->bytes[addr];

    case 0x40000000:    // Peripheral
        addr -= 0x40000000;
        assert(addr < cpu->ram->capacity); // XXX: fault instead
        return (uintptr_t)&cpu->ram->bytes[addr];

//    case 0xE0000000:    // System
//        break;

    default:
        assert(0 && "unhandled memory access"); // XXX: fault instead
    }
}

static uintptr_t getReadAddr(CortexMCore *cpu, uint32_t addr, unsigned size)
{
    if (!cmAddrIsAligned(addr, size)) {
//        then ExceptionTaken(HardFault);
        assert(false);
    }

    cmAddressDescriptor ad;
    cmValidateAddress(&ad, addr, cmModeIsPrivileged(cpu), false, false);

    return mem(cpu, ad.physAddr);
}

uint8_t armv6mRead8(CortexMCore *cpu, uint32_t addr)
{
    uint8_t *p = (uint8_t*)getReadAddr(cpu, addr, 1);
    return *p;
}

uint16_t armv6mRead16(CortexMCore *cpu, uint32_t addr)
{
    uint16_t *p = (uint16_t*)getReadAddr(cpu, addr, 2);
    return *p;
}

uint32_t armv6mRead32(CortexMCore *cpu, uint32_t addr)
{
    uint32_t *p = (uint32_t*)getReadAddr(cpu, addr, 4);
    return *p;
}


static uintptr_t getWriteAddr(CortexMCore *cpu, uint32_t addr, unsigned size)
{
    if (!cmAddrIsAligned(addr, size)) {
//        then ExceptionTaken(HardFault);
        assert(false);
    }

    cmAddressDescriptor ad;
    cmValidateAddress(&ad, addr, cmModeIsPrivileged(cpu), true, false);

    if (ad.memAttrs.shareable) {
//        ClearExclusiveByAddress(ad.physAddr, ProcessorID(), size);
    }

    return mem(cpu, ad.physAddr);
}

void armv6mWrite8(CortexMCore *cpu, uint32_t addr, uint8_t data)
{
    uint8_t *p = (uint8_t *)getWriteAddr(cpu, addr, 1);
    *p = data;
}

void armv6mWrite16(CortexMCore *cpu, uint32_t addr, uint16_t data)
{
    uint16_t *p = (uint16_t*)getWriteAddr(cpu, addr, 2);
    *p = data;
}

void armv6mWrite32(CortexMCore *cpu, uint32_t addr, uint32_t data)
{
    uint32_t *p = (uint32_t*)getWriteAddr(cpu, addr, 4);
    *p = data;
}
