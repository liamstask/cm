#ifndef THUMB2_EMU_H
#define THUMB2_EMU_H

#include <stdint.h>

#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif

enum cmInstrSize {
    InstrBits16,
    InstrBits32
};

static inline enum cmInstrSize instructionSize(uint16_t instr) {
    // if bits [15:11] are 0b11101, 0b11110 or 0b11111, it's a 32-bit instruction
    const uint16_t tst = 0b11101 << 11;
    return (instr & tst) >= tst ? InstrBits32 : InstrBits16;
}

static inline reg_t calculateBranchOffset(reg_t pc, int32_t offset) {
    return (pc + offset + 2) & ~(reg_t)1;
}

void emulateLSLImm(CortexMCore *cpu, uint16_t inst);
void emulateLSRImm(CortexMCore *cpu, uint16_t inst);
void emulateASRImm(CortexMCore *cpu, uint16_t instr);
void emulateADDReg(CortexMCore *cpu, uint16_t instr);
void emulateSUBReg(CortexMCore *cpu, uint16_t instr);
void emulateADD3Imm(CortexMCore *cpu, uint16_t instr);
void emulateSUB3Imm(CortexMCore *cpu, uint16_t instr);
void emulateMovImm(CortexMCore *cpu, uint16_t instr);
void emulateCmpImm(CortexMCore *cpu, uint16_t instr);
void emulateADD8Imm(CortexMCore *cpu, uint16_t instr);
void emulateSUB8Imm(CortexMCore *cpu, uint16_t instr);

void emulateANDReg(CortexMCore *cpu, uint16_t instr);
void emulateEORReg(CortexMCore *cpu, uint16_t instr);
void emulateLSLReg(CortexMCore *cpu, uint16_t instr);
void emulateLSRReg(CortexMCore *cpu, uint16_t instr);
void emulateASRReg(CortexMCore *cpu, uint16_t instr);
void emulateADCReg(CortexMCore *cpu, uint16_t instr);
void emulateSBCReg(CortexMCore *cpu, uint16_t instr);
void emulateRORReg(CortexMCore *cpu, uint16_t instr);
void emulateTSTReg(CortexMCore *cpu, uint16_t instr);
void emulateRSBImm(CortexMCore *cpu, uint16_t instr);
void emulateCMPReg(CortexMCore *cpu, uint16_t instr);
void emulateCMNReg(CortexMCore *cpu, uint16_t instr);
void emulateORRReg(CortexMCore *cpu, uint16_t instr);
void emulateMUL(CortexMCore *cpu, uint16_t instr);
void emulateBICReg(CortexMCore *cpu, uint16_t instr);
void emulateMVNReg(CortexMCore *cpu, uint16_t instr);

void emulateSXTH(CortexMCore *cpu, uint16_t instr);
void emulateSXTB(CortexMCore *cpu, uint16_t instr);
void emulateUXTH(CortexMCore *cpu, uint16_t instr);
void emulateUXTB(CortexMCore *cpu, uint16_t instr);
void emulateMOV(CortexMCore *cpu, uint16_t instr);

void emulateB(CortexMCore *cpu, uint16_t instr);
void emulateBL(CortexMCore *cpu, uint32_t instr);
void emulateBLX(CortexMCore *cpu, uint16_t instr);
void emulateBX(CortexMCore *cpu, uint16_t instr);
void emulateCondB(CortexMCore *cpu, uint16_t instr);
void emulateCBZ_CBNZ(CortexMCore *cpu, uint16_t instr);

void emulateSTRSPImm(CortexMCore *cpu, uint16_t instr);
void emulateLDRSPImm(CortexMCore *cpu, uint16_t instr);
void emulateADDSpImm(CortexMCore *cpu, uint16_t instr);
void emulateLDRLitPool(CortexMCore *cpu, uint16_t instr);

void emulateSTRReg(CortexMCore *cpu, uint16_t instr);
void emulateSTRHReg(CortexMCore *cpu, uint16_t instr);
void emulateSTRBReg(CortexMCore *cpu, uint16_t instr);
void emulateLDRSBReg(CortexMCore *cpu, uint16_t instr);
void emulateLDRReg(CortexMCore *cpu, uint16_t instr);
void emulateLDRHReg(CortexMCore *cpu, uint16_t instr);
void emulateLDRBReg(CortexMCore *cpu, uint16_t instr);
void emulateLDRSHReg(CortexMCore *cpu, uint16_t instr);

void emulateSTR(CortexMCore *cpu, uint32_t instr);
void emulateLDR(CortexMCore *cpu, uint32_t instr);
void emulateSTRBH(CortexMCore *cpu, uint32_t instr);
void emulateLDRBH(CortexMCore *cpu, uint32_t instr);
void emulateMOVWT(CortexMCore *cpu, uint32_t instr);
void emulateDIV(CortexMCore *cpu, uint32_t instr);
void emulateCLZ(CortexMCore *cpu, uint32_t instr);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // THUMB2_EMU_H
