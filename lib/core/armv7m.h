#ifndef ARMV7M_H
#define ARMV7M_H

#include "cortex-m-core.h"

#ifdef __cplusplus
extern "C" {
#endif

void cmInitARMv7M(CortexMCore *cpu);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ARMV7M_H
