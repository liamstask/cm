#ifndef VTIME_H
#define VTIME_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Tracks a virtual clock, measured in clock ticks.
 */
typedef struct VirtualTime_t {
    unsigned targetRate;
    uint64_t ticks;
    uint32_t rateInHz;  // system clock rate, in Hz
    unsigned timestep;  // timestep size for simulation, in real milliseconds
} VirtualTime;

// singleton getter
VirtualTime *cmSystemTime();

// conversion helpers
uint64_t vTimeMsec(const VirtualTime *v, uint64_t u);
uint64_t vTimeUsec(const VirtualTime *v, uint64_t u);
uint64_t vTimeNsec(const VirtualTime *v, uint64_t n);
uint64_t vTimeHz(const VirtualTime *v, unsigned h);
double vTimeToSeconds(const VirtualTime *v, uint64_t cycles);
double vTimeClockMHZ(const VirtualTime *v);

// instance methods
void vTimeInit(VirtualTime *v, uint32_t rateInHz, unsigned timestep);
void vTimeInitSystem(uint32_t rateInHz, unsigned timestep);
void vTimeTick(VirtualTime *v, unsigned count);
double vTimeElapsedSeconds(const VirtualTime *v);
void vTimeSetTargetRate(VirtualTime *v, unsigned realTimeHz);
void vTimeRun(VirtualTime *v);
void vTimeStop(VirtualTime *v);
bool vTimeIsPaused(const VirtualTime *v);
unsigned vTimeTimestepTicks(const VirtualTime *v);

/*
 * A timer which measures elapsed virtual and real time, from a
 * common reference point.
 */
typedef struct ElapsedTime_t {
    const VirtualTime *vtime;
    uint64_t virtualT;
    double realS;
    double currentRealS;
} ElapsedTime;


void     eTimeInit(ElapsedTime *e, const VirtualTime *vtime);
void     eTimeCapture(ElapsedTime *e);
void     eTimeCaptureFrom(ElapsedTime *e, const ElapsedTime *source);
void     eTimeStart(ElapsedTime *e);
uint64_t eTimeVirtualTicks(const ElapsedTime *e);
uint64_t eTimeRealMsec(const ElapsedTime *e);
double   eTimeVirtualSeconds(const ElapsedTime *e);
double   eTimeRealSeconds(const ElapsedTime *e);
double   eTimeVirtualRatio(const ElapsedTime *e);

/*
 * The time governor slows us down if we're running faster than
 * real-time, by keeping a long-term average of how many seconds
 * ahead or behind we are.
 */
typedef struct TimeGovernor_t {
    ElapsedTime et;
    double secondsAhead;
} TimeGovernor;


void timeGovStart(TimeGovernor *g, const VirtualTime *vtime);
void timeGovStep(TimeGovernor *g);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // VTIME_H
