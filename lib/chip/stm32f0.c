#include "stm32f0.h"
#include "core/cortex-m.h"
#include "core/armv6m.h"

bool stm32F0Init(struct Stm32F0 *s, const char *firmwarePath)
{
    cmInitARMv6M(&s->cpu);

    // XXX: provide config options

    s->cpu.flash = &s->flash;
    s->flash.capacity = 1024 * 128;
    if (!cmMemInit(&s->flash, firmwarePath)) {
        return false;
    }

    s->cpu.ram = &s->ram;
    s->ram.capacity = 1024 * 32;
    if (!cmMemInit(&s->ram, 0)) {
        return false;
    }

    if (!cmReset(&s->cpu)) {
        return false;
    }

    return true;
}

void stm32F0Tick(struct Stm32F0 *s)
{
    cmExecute(&s->cpu);
}
