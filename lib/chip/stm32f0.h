#ifndef STM32F0_H
#define STM32F0_H

#ifdef __cplusplus
extern "C" {
#endif

#include "core/cortex-m-core.h"
#include "memory.h"

struct Stm32F0 {
    CortexMCore cpu;
    struct Memory flash;
    struct Memory ram;
};

bool stm32F0Init(struct Stm32F0 *s, const char *firmwarePath);
void stm32F0Tick(struct Stm32F0 *s);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // STM32F0_H
