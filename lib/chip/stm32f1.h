#ifndef STM32F1_H
#define STM32F1_H

#ifdef __cplusplus
extern "C" {
#endif

#include "core/cortex-m-core.h"
#include "memory.h"

struct Stm32F1 {
    CortexMCore cpu;
    struct Memory flash;
    struct Memory ram;
};

bool stm32F1Init(struct Stm32F1 *s, const char *firmwarePath);
void stm32F1Tick(struct Stm32F1 *s);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // STM32F1_H
