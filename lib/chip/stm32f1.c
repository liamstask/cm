#include "stm32f1.h"
#include "core/cortex-m.h"
#include "core/armv7m.h"

bool stm32F1Init(struct Stm32F1 *s, const char *firmwarePath)
{
    cmInitARMv7M(&s->cpu);

    // XXX: provide config options

    s->cpu.flash = &s->flash;
    s->flash.capacity = 1024 * 128;
    if (!cmMemInit(&s->flash, firmwarePath)) {
        return false;
    }

    s->cpu.ram = &s->ram;
    s->ram.capacity = 1024 * 32;
    if (!cmMemInit(&s->ram, 0)) {
        return false;
    }

    if (!cmReset(&s->cpu)) {
        return false;
    }

    return true;
}

void stm32F1Tick(struct Stm32F1 *s)
{
    cmExecute(&s->cpu);
}
