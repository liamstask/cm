#include "cm.h"
#include "ostime.h"
#include "vtime.h"

void cmInit(uint32_t clockRateHz, unsigned timestep)
{
    /*
     * One-time initialization that should
     * happen on start up.
     */

    osTimeInit();
    vTimeInitSystem(clockRateHz, timestep);
}
