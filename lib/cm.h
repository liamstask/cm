
#ifndef _CM_H_
#define _CM_H_

#include "memory.h"
#include "vtime.h"

#include "core/armv6m.h"
#include "core/armv7m.h"

#include "chip/stm32f0.h"
#include "chip/stm32f1.h"

void cmInit(uint32_t clockRateHz, unsigned timestep);

#endif // _CM_H_
