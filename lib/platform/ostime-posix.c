#include "ostime.h"

#include <stdint.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>

/*
 * posix implementation of osTime
 */

static struct timeval epoch;

void osTimeInit()
{
    /*
     * Should only be called once.
     */

    int rv = gettimeofday(&epoch, 0);
    assert(rv == 0 && "gettimeofday() failure");
}

inline double osTimeClock()
{
    struct timeval now;
    int rv = gettimeofday(&now, 0);
    assert(rv == 0 && "gettimeofday() failure");

    return (now.tv_sec - epoch.tv_usec) +
        1e-6 * (now.tv_usec - epoch.tv_usec);
}

inline void osTimeSleep(double seconds)
{
    struct timespec tv;
    tv.tv_sec = seconds;
    tv.tv_nsec = (seconds - tv.tv_sec) * 1e9;
    int rv = nanosleep(&tv, 0);
    assert(rv == 0 && "nanosleep() failure");
}
