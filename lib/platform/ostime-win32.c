#include "ostime.h"

#include <windows.h>
#include <mmsystem.h>
#include <stdint.h>

/*
 * windows implementation of osTime
 */

struct osTimeData {
    bool hasQPC;
    double toSeconds;
};

static struct osTimeData timeData;

void osTimeInit()
{
    /*
     * Should only be called once.
     */

    LARGE_INTEGER freq;
    if (QueryPerformanceFrequency(&freq)) {
        timeData.hasQPC = true;
        timeData.toSeconds = 1.0 / (double)freq.QuadPart;
    } else {
        timeData.hasQPC = false;
        printf("WARNING: No high-resolution timer available!\n");
    }

    // Ask for 1ms resolution in Sleep(), rather than the default 10ms!
    timeBeginPeriod(1);
}

inline double osTimeClock()
{
    if (data.hasQPC) {
        LARGE_INTEGER t;
        if (!QueryPerformanceCounter(&t)) {
            printf("WARNING: QueryPerformanceCounter() error\n");
        }
        return t.QuadPart * timeData.toSeconds;
    }

    return GetTickCount() * 1e-3;
}

inline void osTimeSleep(double seconds)
{
    int ms = seconds * 1000 - 1;
    if (ms >= 1) {
        Sleep(ms);
    }
}
