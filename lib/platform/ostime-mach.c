#include "ostime.h"

#include <stdint.h>
#include <assert.h>
#include <mach/mach_time.h>

/*
 * OS X implementation of osTime
 */

struct osTimeData {
    double toSeconds;
    uint64_t epoch;
};

static struct osTimeData timeData;

void osTimeInit()
{
    /*
     * Should only be called once.
     */

    mach_timebase_info_data_t info;
    kern_return_t rv = mach_timebase_info(&info);
    assert(rv == KERN_SUCCESS && "failed to get mach_timebase_info()");

    timeData.toSeconds = (1e-9 * info.numer) / info.denom;
    timeData.epoch = mach_absolute_time();
}

inline double osTimeClock()
{
    return (mach_absolute_time() - timeData.epoch) * timeData.toSeconds;
}

inline void osTimeSleep(double seconds)
{
    mach_wait_until(mach_absolute_time() + seconds / timeData.toSeconds);
}
