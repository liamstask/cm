#include "vtime.h"
#include "ostime.h"

#include <stdio.h>

//////////////////////////////////////////////////
//                  Virtual Time
//////////////////////////////////////////////////

static VirtualTime systemVTime;

void vTimeInitSystem(uint32_t rateInHz, unsigned timestep)
{
    /*
     * Prelim init for system time.
     *
     * This may be overridden during init as firmware
     * configures the system clocks, etc.
     */

    vTimeInit(&systemVTime, rateInHz, timestep);
}

VirtualTime *cmSystemTime()
{
    return &systemVTime;
}

uint64_t vTimeMsec(const VirtualTime *v, uint64_t u)
{
    return v->rateInHz * u / 1000ULL;
}

uint64_t vTimeUsec(const VirtualTime *v, uint64_t u)
{
    return v->rateInHz * u / 1000000ULL;
}

uint64_t vTimeNsec(const VirtualTime *v, uint64_t n)
{
    return v->rateInHz * n / 1000000000ULL;
}

uint64_t vTimeHz(const VirtualTime *v, unsigned h)
{
    return v->rateInHz / h;
}

double vTimeToSeconds(const VirtualTime *v, uint64_t cycles)
{
    return cycles / (double)v->rateInHz;
}

double vTimeClockMHZ(const VirtualTime *v)
{
    return v->rateInHz / 1e6;
}

void vTimeInit(VirtualTime *v, uint32_t rateInHz, unsigned timestep)
{
    v->ticks = 0;
    v->rateInHz = rateInHz;
    v->timestep = timestep;
    vTimeRun(v);
}

inline void vTimeTick(VirtualTime *v, unsigned count)
{
    v->ticks += count;
}

double vTimeElapsedSeconds(const VirtualTime *v)
{
    return vTimeToSeconds(v, v->ticks);
}

void vTimeSetTargetRate(VirtualTime *v, unsigned realTimeHz)
{
    v->targetRate = realTimeHz;
}

void vTimeRun(VirtualTime *v)
{
    vTimeSetTargetRate(v, v->rateInHz);
}

void vTimeStop(VirtualTime *v)
{
    vTimeSetTargetRate(v, 0);
}

bool vTimeIsPaused(const VirtualTime *v)
{
    return v->targetRate == 0;
}

unsigned vTimeTimestepTicks(const VirtualTime *v)
{
    /*
     * How many ticks fit into TIMESTEP given
     * the current targetRate?
     */
    unsigned t = v->timestep * v->targetRate / 1000;
    return t ? t : 1;
}


//////////////////////////////////////////////////
//                  Elapsed Time
//////////////////////////////////////////////////


void eTimeInit(ElapsedTime *e, const VirtualTime *vtime)
{
    e->vtime = vtime;
    eTimeCapture(e);
}

void eTimeCapture(ElapsedTime *e)
{
    e->currentRealS = osTimeClock();
}

void eTimeCaptureFrom(ElapsedTime *e, const ElapsedTime *source)
{
    /*
     * Allows optimization to reduce the number of system calls
     * when many timers are captured at the same time.
     */
    e->currentRealS = source->currentRealS;
}

void eTimeStart(ElapsedTime *e)
{
    e->virtualT = e->vtime->ticks;
    e->realS = e->currentRealS;
}

uint64_t eTimeVirtualTicks(const ElapsedTime *e)
{
    return e->vtime->ticks - e->virtualT;
}

uint64_t eTimeRealMsec(const ElapsedTime *e)
{
    return eTimeRealSeconds(e) * 1e3;
}

double eTimeVirtualSeconds(const ElapsedTime *e)
{
    return eTimeVirtualTicks(e) / (double)e->vtime->rateInHz;
}

double eTimeRealSeconds(const ElapsedTime *e)
{
    return e->currentRealS - e->realS;
}

double eTimeVirtualRatio(const ElapsedTime *e)
{
    double rs = eTimeRealSeconds(e);
    return rs ? eTimeVirtualSeconds(e) / rs : 0;
}


//////////////////////////////////////////////////
//                  Time Governor
//////////////////////////////////////////////////


void timeGovStart(TimeGovernor *g, const VirtualTime *vtime)
{
    eTimeInit(&g->et, vtime);
    eTimeStart(&g->et);
    g->secondsAhead = 0.0;
}

void timeGovStep(TimeGovernor *g)
{
    /*
     * How fast are we running, on average? Keep an error value,
     * indicating how far ahead or behind real-time we're
     * running. If we need to slow down, we can insert a delay
     * here.
     *
     * If we're really far behind, give up on running full
     * speed. But if we're only a little bit behind, try to catch
     * up.
     *
     * We assume that our virtual time is being ticked in accordance
     * with vTimeTimestepTicks() between each step.
     */

    static const double maxDeviation = 0.75;
    static const double minSleep = 0.01;

    eTimeCapture(&g->et);

    g->secondsAhead += eTimeVirtualSeconds(&g->et) - eTimeRealSeconds(&g->et);

    /*
     * Fully reset if we're lagging/leading too much, rather than clamping to our limit.
     * (Among other things, this avoids catch-up delay when coming out of Turbo)
     */

    if (g->secondsAhead < -maxDeviation || g->secondsAhead > maxDeviation) {
        g->secondsAhead = 0;
    }

    if (g->secondsAhead > minSleep) {
        osTimeSleep(g->secondsAhead);
    }

    eTimeStart(&g->et);
}
