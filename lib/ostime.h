#ifndef OSTIME_H
#define OSTIME_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Utilities for working with the host operating system's notion of time.
 *
 * Retrieve the system's monotonic high-res timer, and perform
 * high-resolution single-thread sleeps.
 */

void osTimeInit();
double osTimeClock();
void osTimeSleep(double seconds);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // OSTIME_H
