#include "memory.h"

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

bool cmMemInit(struct Memory *m, const char *filepath)
{
    /*
     * filepath is the file used to back this memory.
     * If no path is specified, back with allocated memory.
     */

    if (!filepath) {
        m->bytes = malloc(m->capacity);
        if (!m->bytes) {
            fprintf(stderr, "MEM - can't allocate backing memory\n");
            return false;
        }
        m->fileHandle = -1;
        return true;
    }

    int fh = open(filepath, O_RDWR, 0777);
    if (fh < 0) {
        fprintf(stderr, "MEM - can't open backing file '%s': %s (%d)\n", filepath, strerror(errno), errno);
        return false;
    }

    struct stat st;
    if (fstat(fh, &st)) {
        close(fh);
        fprintf(stderr, "MEM - can't access backing file '%s': %s (%d)\n", filepath, strerror(errno), errno);
        return false;
    }

    if ((unsigned)st.st_size < m->capacity && ftruncate(fh, m->capacity)) {
        close(fh);
        fprintf(stderr, "MEM - can't resize backing file '%s': %s (%d)\n", filepath, strerror(errno), errno);
        return false;
    }

    void *mapping = mmap(NULL, m->capacity, PROT_READ | PROT_WRITE, MAP_SHARED, fh, 0);
    if (mapping == MAP_FAILED) {
        close(fh);
        fprintf(stderr, "MEM - can't memory-map backing file '%s' (%s)\n", filepath, strerror(errno));
        return false;
    }

    m->fileHandle = fh;
    m->bytes = mapping;
    return true;
}

void cmMemDeinit(struct Memory *m)
{
    if (m->fileHandle == -1) {
        free(m->bytes);
        return;
    }

    if (m->fileHandle >= 0) {
        if (fsync(m->fileHandle)) {
            fprintf(stderr, "MEM - could not sync to disk: %s (%d)\n", strerror(errno), errno);
        }
        if (munmap(m->bytes, m->capacity)) {
            fprintf(stderr, "MEM - could not unmap: %s (%d)\n", strerror(errno), errno);
        }
        close(m->fileHandle);
    }
}
